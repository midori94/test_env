import http.client
import urllib
import base64
import cv2

HOST = '192.168.1.100'
URL =  '/api/v1/set_alarm'
HEADERS = {
        "Content-type": "application/x-www-form-urlencoded",
        "Accept": "text/plain"
        }

class Sender:

    def __init__(self, host = HOST, url = URL):
        self.host = host
        self.url = url

    def sendpackage(self, image, idx, alarm, sco_number, date, time):
        jpg_image = cv2.imencode('.jpg', image)[1]
        b64_image = base64.b64encode(jpg_image).decode('ascii')

        params = urllib.parse.urlencode({ 
            'image': b64_image,
            'idx': idx,
            'alarm': alarm,
            'sco_number': sco_number,
            'date': date,
            'time': time
            })

        conn = http.client.HTTPConnection(self.host)
        conn.request('POST', self.url, params, HEADERS)
        return conn.getresponse().read().decode('ascii')


if __name__ == '__main__':
    img = cv2.imread('test.jpg')
    sender = Sender()
    print(sender.sendpackage(img, 1, 1, 1, '2020-02-26', '15:32.50'))


