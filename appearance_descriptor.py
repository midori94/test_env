#!.venv/bin/python
from skimage.feature import local_binary_pattern, hog
from skimage.color import rgb2gray
import numpy as np

class LBPHistCalculator:

    def __init__(self, points = 8, radius = 1, bins = 32):
        self.points = points
        self.radius = radius
        self.bins = bins

    def process(self, image, mask = None):
        gray_scale_image = rgb2gray(image)
        lbp_image = local_binary_pattern(gray_scale_image, self.points, self.radius, 
                method = 'uniform')[mask]
        hist, _ = np.histogram(lbp_image.ravel(), bins = self.bins,
                range = (0, 2**self.points - 1), density = True)
        return hist

class RGBHistCalculator:

    def __init__(self, bins = 32):
        self.bins = bins

    def process(self, image, mask = None):
        channels = np.transpose(image, (2,0,1))
        hists = []
        for channel in channels:
            hist, _ = np.histogram(channel[mask].ravel(), bins = self.bins, range = (0, 255), density = True)
            hists.append(hist)
        return np.hstack(hists)

class HOGCalculator:

    def __init__(self, bins = 8, cells = (2,2)):
        self.bins = bins
        self.cells = cells

    def process(self, image, mask = None):
        dim = image.shape[:2]
        pixels = ( dim[0] // self.cells[0], dim[1] // self.cells[1])
        hist = hog(image, orientations = self.bins, pixels_per_cell = pixels, 
                cells_per_block = self.cells, feature_vector = True, multichannel = True)
        return hist

class AppearanceDescriptor:

    def __init__(self, feature_modules):
        self.modules = feature_modules

    def describe(self, image, mask = None):
        feature_list = []
        for module in self.modules:
            feature_list.append(module.process(image))
        feature_vector = np.hstack(feature_list)
        return feature_vector
                
if __name__ == "__main__":
    from skimage.io import imread, imshow, show
    import matplotlib.pyplot as plt

    image = imread("test.jpg")

    feature_modules = (
            LBPHistCalculator(),
            RGBHistCalculator(),
            HOGCalculator()
            )

    appearance_descriptor = AppearanceDescriptor(feature_modules)
    feature_vector = appearance_descriptor.describe(image)


