#!.venv/bin/python
from appearance_descriptor import *
from pickle import load
import numpy as np
import cv2

def fetch_model(model_path):
    with open(model_path, 'rb') as f:
        model = load(f)
    return model

class Classifier:

    def __init__(self, appearance_descriptor, pca, clf, scaler = None):
        self.appearance_descriptor = appearance_descriptor
        self.pca = pca
        self.scaler = scaler
        self.clf = clf

    def is_shadow(self, image, mask):
        image_description = self.appearance_descriptor.describe(
                image, 
                mask
        ).reshape(1,-1)
        if self.scaler:
            image_description = self.scaler.transform(image_description)
        projected_description = self.pca.transform(image_description)
        prediction = self.clf.predict(projected_description)
        return prediction

if __name__ == '__main__':
    DATA_FILE = 'data/test.data'
    SCALER_MODEL_FILE = 'data/model.scaler'
    PCA_MODEL_FILE = 'data/model.pca'
    CLF_MODEL_FILE = 'data/model.clf'

    feature_modules = (
            LBPHistCalculator(),
            RGBHistCalculator(),
            HOGCalculator()
    )
    appearance_descriptor = AppearanceDescriptor(feature_modules)

    shadow_classifier = Classifier(
            appearance_descriptor,
            fetch_model(PCA_MODEL_FILE),
            fetch_model(CLF_MODEL_FILE),
            scaler = fetch_model(SCALER_MODEL_FILE)
    )

    with open(DATA_FILE, 'rb') as f:
        data = load(f)

    for image, mask, label in data:
        if shadow_classifier.is_shadow(image, mask):
            print(f'1 {label}')
        else:
            print(f'0 {label}')


            


