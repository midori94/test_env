#!.venv/bin/python
from sklearn.preprocessing import StandardScaler
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA
from skimage.io import imshow, show
from appearance_descriptor import *
import matplotlib.pyplot as plt
from sklearn.svm import SVC
from pickle import load, dump
import numpy as np

DATA_FILE = 'data/test.data'
SCALER_MODEL_FILE = 'data/model.scaler'
PCA_MODEL_FILE = 'data/model.pca'
CLF_MODEL_FILE = 'data/model.clf'

with open(DATA_FILE, 'rb') as f:
    data = load(f)

feature_modules = (
        LBPHistCalculator(),
        RGBHistCalculator(),
        HOGCalculator()
        )

appearance_descriptor = AppearanceDescriptor(feature_modules)
learning_list = []
labels_list = []
for img, mask, label in data:
    learning_list.append(appearance_descriptor.describe(img, mask))
    labels_list.append(label)
learning_set = np.stack(learning_list)
labels = np.array(labels_list)

scaler = StandardScaler()
normalized_learning_set = scaler.fit_transform(learning_set)

pca = PCA(n_components=3)
projected_learning_set = pca.fit_transform(normalized_learning_set)

clf = SVC()
clf.fit(projected_learning_set, labels)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
for label, vector in zip(labels, learning_set):
    transformed_vector = pca.transform(scaler.transform(vector.reshape(1,-1)))
    color = 'r' if label else 'b'
    ax.scatter(transformed_vector[0,0], transformed_vector[0,1], transformed_vector[0,2], 
            c=color, marker='.')

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
for vector in learning_set:
    transformed_vector = pca.transform(scaler.transform(vector.reshape(1,-1)))
    prediction = clf.predict(transformed_vector)
    color = 'r' if prediction else 'b'
    ax.scatter(transformed_vector[0,0], transformed_vector[0,1], transformed_vector[0,2], 
            c=color, marker='.')

plt.show()

with open(SCALER_MODEL_FILE, 'wb') as f:
    dump(scaler, f)

with open(PCA_MODEL_FILE, 'wb') as f:
    dump(pca, f)

with open(CLF_MODEL_FILE, 'wb') as f:
    dump(clf, f)

