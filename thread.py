from threading import Thread

class Thread0(Thread):
    
    def __init__(self):
        Thread.__init__(self)
        self.string = 'Hola'

    def run(self):
        while True:
            print(self.string)


class Thread1(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.string = 'Chao'

    def run(self):
        while True:
            print(self.string)

if __name__ == '__main__':
    thread0 = Thread0()
    thread1 = Thread1()
    thread0.start()
    thread1.start()
