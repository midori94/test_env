#!.venv/bin/python
import csv

def load_detections(f):

    csv_list = csv.reader(f, quoting = csv.QUOTE_NONNUMERIC)
    detections = []
    for frame_idx, det_idx, x, y, w, h, config, *_ in csv_list:
        if config:
            detection = (
                    int(det_idx),
                    (int(x), int(y)),
                    (int(x+w), int(y+h))
            )
            try:
                detections[int(frame_idx) - 1].append(detection)
            except:
                detections.append([detection])
    return detections

if __name__ == '__main__':
    import numpy as np
    import cv2
    
    cv2.namedWindow('video', cv2.WINDOW_NORMAL)

    with open('data/test_video/gt/gt.txt', 'r') as f:          
        detections = load_detections(f)

    get_frame = lambda i: cv2.imread(f'data/test_video/img1/{1:06}.jpg')

    i = 1
    frame = get_frame(i)
    while frame is not None:
        for idx, pt1, pt2 in detections.pop(0):
            cv2.rectangle(frame, pt1, pt2, (0,255,0))
        cv2.imshow('video', frame)
        key = cv2.waitKey(1)
        if key == ord('q'):
            exit(0)

        i  += 1
        frame = get_frame(i)


