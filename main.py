#!.venv/bin/python
from sklearn.preprocessing import StandardScaler
from detection_loader import load_detections
from sklearn.decomposition import PCA
from appearance_descriptor import *
import numpy as np
from pickle import load, dump
import cv2
import csv

def crop(frame, pt1, pt2):
    cropped_frame = frame[pt1[1]:pt2[1], pt1[0]:pt2[0]]
    return cropped_frame

with open('data/test_video/gt/gt.txt', 'r') as f:          
    detections = load_detections(f)

with open('data/model.scaler', 'rb') as f:
    scaler = load(f)

with open('data/model.pca', 'rb') as f:
    pca = load(f)

feature_modules = (
        LBPHistCalculator(),
        RGBHistCalculator(),
        HOGCalculator()
)
appearance_descriptor = AppearanceDescriptor(feature_modules)

get_frame = lambda i: cv2.imread(f'data/test_video/img1/{i:06}.jpg')

i = 1
frame = get_frame(i)
while frame is not None:
    projections = []
    for idx, pt1, pt2 in detections.pop(0):
        cropped_detection = crop(frame, pt1, pt2)
        try:
            detection_descriptor = appearance_descriptor.describe(
                cropped_detection
            )
            projection = pca.transform(
                    scaler.transform(
                        detection_description
                    )
            )
            projections.append(projection)
        except:
            pass

    i  += 1
    frame = get_frame(i)


#scaler = StandardScaler()
#normalized_data = scaler.fit_transform(data)
#
#pca = PCA(n_components = 2)
#pca.fit(normalized_data)
#
#with open('data/model.scaler', 'wb') as f:
#    dump(scaler, f)
#
#with open('data/model.pca', 'wb') as f:
#    dump(pca, f)
